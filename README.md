Nektar++/tutorial
========
Nektar++/tutorial contains some useful tutorials associated to the open-source 
software framework Nektar++. 

The software is designed to support the development of high-performance scalable
solvers for partial differential equations (PDEs) using the spectral/hp element 
method.

The tutorials are still under development and will become available shortly.

Note that the User Guide is available for download from <http://www.nektar.info/>.
