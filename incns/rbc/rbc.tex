%%%%%%%%%%%% DOCUMENT BEGINS %%%%%%%%%%%%%%%%%%%%%
\input{tutorial-preamble.tex}

\usepackage{subcaption}
\captionsetup{compatibility=false}


\author{Department of Aeronautics, Imperial College London, UK\newline
Scientific Computing and Imaging Institute, University of Utah, USA}
\title{Rayleigh--B\'enard  Convection}
\date{\today}

\begin{document}

% Render pretty title page if not building HTML
\ifdefined\HCode
\begin{center}
    \includegraphics[width=0.1\textwidth]{img/icon-blue.png}
\end{center}
\maketitle
\begin{center}
    \huge{Tutorials}
\end{center}
\else
\titlepage
\fi

\clearpage

\chapter{Introduction}
This tutorial provides an overview of utilising the spectral/hp element framework {\em Nektar++} for the numerical solution of a classical fluid dynamics problem: Rayleigh--B\'enard Convection (RBC). The tutorial will delve into the direct numerical simulation and linear stability analysis of a two-dimensional RBC.

Information on how to install the libraries, solvers, and utilities on your own computer is available on the webpage \href{http://www.nektar.info}{\underline{www.nektar.info}}. This tutorial assumes the reader has already completed the previous tutorial in the Flow Stability series on the channel and already has the necessary software installed.
%
\begin{tutorialtask}
Prepare for the tutorial. Make sure that you have:
\begin{itemize}
   \item Downloaded the tutorial files: 
       \relurl{rbc.tar.gz}{rbc}\\
   Unpack it using
   \inlsh{unzip rbc.tar.gz}
   to produce a directory \inlsh{rbc} with subdirectories called \inlsh{tutorial} and
   \inlsh{complete}.
   We will refer to the \inlsh{tutorial} directory
   as \inlsh{\$NEKTUTORIAL}. 
\end{itemize}
\end{tutorialtask}
%
\section*{Governing equations}
The dynamical equations that describe Rayleigh--B\'enard  Convection (RBC) under Boussinesq approximation in the non-dimensional form are
\begin{align} 
\frac{\partial \mathbf{u}}{\partial t} + \mathbf{u} \cdot \nabla{\mathbf{u}} 
        &=-\nabla p + RaPrT \mathbf{e}_y+ Pr\nabla^{2}\mathbf{u},\\ \label{eq:u}
        \frac{\partial T}{\partial t} + \mathbf{u} \cdot 
        \nabla{T} 
        &=\nabla^{2}T,\\
\nabla \cdot \mathbf{u}&=0 \label{eq:div_u},
\end{align}
where $\mathbf{u}$ and $T$ are the velocity and temperature fields, respectively, and $\mathbf{e}_y$ is the buoyancy direction. Here $p$ is the modified pressure including the buoyancy term accounting for the reference temperature. Flow is controlled by two non-dimensional numbers, the Rayleigh number $Ra$ and the Prandtl number $Pr$.

By decomposing the velocity, temperature and pressure in the  equations (\ref{eq:u}-\ref{eq:div_u}) as a summation of a base flow $(\mathbf{U}, \bar{T}, P)$ and perturbation $(\mathbf{u}^\prime, T^\prime, p^\prime)$, such that 
\begin{align}
\mathbf{u} &= \mathbf{U} + \mathbf{u}^\prime \\
T &= \bar{T} +  T^\prime \\
p &= P +  p^\prime,
\end{align}
and retaining first-order terms only yields the linearised equations governing the evolution of infinitesimal perturbations:
\begin{align} 
\frac{\partial \mathbf{u}^\prime}{\partial t} + \mathbf{u}^\prime \cdot \nabla{\mathbf{U}} + (\mathbf{U}\cdot \nabla )\mathbf{u}^\prime 
        &=-\nabla p^\prime + RaPrT^\prime \mathbf{e}_y+ Pr\nabla^{2}\mathbf{u}^\prime,\\ \label{eq:u_prime}
        \frac{\partial T^\prime}{\partial t} + \mathbf{u^\prime} \cdot 
        \nabla \bar{T} +\mathbf{U} \cdot 
        \nabla T^\prime 
        &=\nabla^{2}T^\prime,\\
\nabla \cdot \mathbf{u}^\prime&=0 \label{eq:div_u_prime}.
\end{align}
%
In Chapter~\ref{RBC:DNS}, we will validate a case of two-dimensional RBC in a domain of aspect ratio $2.02$. In Chapter~\ref{RBC:LSA}, we will discuss the linear stability analysis for a no-slip boundary condition at top and bottom plates, and will estimate the critical value of Rayleigh number. 
%
The files contained in the \texttt{\$NEKTUTORIAL} directory are as follows:
\begin{itemize}
\item Folder \texttt{geometry}
\begin{itemize}
\item  \texttt{rbc.geo} - \gmsh file that contains the geometry of the problem.
\item \texttt{rbc.msh} - \gmsh generated mesh data listing mesh vertices and 
elements.
\end{itemize}
%
\item Folder \texttt{DNS/Ra\_5e3\_Pr\_0p71}
\begin{itemize}
\item \texttt{rbc-DNS.xml} - \nektar session file, generated with the
\texttt{\$NEK/NekMesh} utility, for computing the flow for $Ra=5000$ and $Pr=0.71$.
\end{itemize}
%
\item Folder \texttt{LSA/Ra\_1900}
\begin{itemize}
\item \texttt{rbc-LSA.xml} - \nektar session file, generated with \texttt{\$NEK/NekMesh} for the linear stability analysis for $Ra=1900$ and $Pr=0.71$.
\end{itemize}
%
\end{itemize}

\section*{Mesh generation}

The first step is to generate a mesh that is readable by \nektar.
The files necessary in this section can be found in \texttt{\$NEKTUTORIAL/geometry/}.
To achieve this task, we use \gmsh in conjunction with the \nektar pre-processing utility
called \texttt{\$NEK/NekMesh}. Specifically, we first generate the mesh in figure
\ref{Channel_mesh} using \gmsh and successively, we convert it into a suitable \nektar
format using \texttt{\$NEK/NekMesh}. For all calculations in this tutorial, we fix $L_x = 2.02$ and $L_y = 1.0$ to maintain the aspect ratio of 2.02.
%
\begin{tutorialtask}
Convert the $Gmsh$ geometry provided into the XML \nektar format and with two periodic
boundaries
\begin{itemize}
\item \texttt{rbc.msh} can be generated using $Gmsh$ by running the following command:
\tutorialcommand{gmsh -2 rbc.geo}
\item \texttt{rbc.xml} can be generated using the module
\texttt{peralign} available with the pre-processing tool \texttt{\$NEK/NekMesh}:
\tutorialcommand{\$NEK/NekMesh -m peralign:surf1=2:surf2=3:dir=x rbc.mesh rbc.xml}
where \texttt{surf1} and \texttt{surf2} correspond to the periodic physical surface
IDs specified in \textit{Gmsh} (in our case the inflow has a physical ID=2 while the outflow
has a physical ID=3) and \texttt{dir} is the periodicity direction (i.e. the direction normal
to the inflow and outflow boundaries - in our case $x$).
\end{itemize}
\end{tutorialtask}
%
\begin{figure}
\centering
\includegraphics[scale=0.9]{img/rbc_geo.pdf}
\caption{100 quadrilaterals mesh}
\label{Channel_mesh}
\end{figure}

%\noindent
%\vspace{5 cm}

\chapter{Direct Numerical Simulation}
\label{RBC:DNS}
%
This chapter will consider two-dimensional Rayleigh--B\'enard  convection in a box of aspect ratio 2.02. At the top and bottom walls, we employ a no-slip boundary condition for the velocity field and a conducting boundary condition for the temperature field. At the sidewalls, we set periodic boundary conditions for both fields. We will compare our results with  Clever and Busse  (J. Fluid Mech., 1974; \textbf{65}:625-645)  for $Ra=5000$, $Pr=0.71$, and $L_x = 2 \pi /3.117=2.02$.  

In the folder \texttt{\$NEKTUTORIAL/DNS/Ra\_5e3\_Pr\_0p71} you will find the file \texttt{rbc-DNS.xml} which contains the geometry along with the necessary  parameters to solve the problem. The \texttt{GEOMETRY} section is responsible for defining the mesh of the problem, and it is automatically generated, as demonstrated in the preceding task. Within the \texttt{EXPANSIONS} section, the expansion type and order are specified.  An expansion basis is applied to a geometric \textit{composite}, where \textit{composite} refers to a collection of mesh entities. The \texttt{\$NEK/NekMesh} utility always includes a default entry, and in this case, the composite \texttt{C[0]} represents the set of all elements. The \texttt{FIELDS} attribute indicates the fields for which this expansion is applicable, and the \texttt{TYPE} attribute designates the type of polynomial basis functions used in the expansion. For example,
%
\begin{lstlisting}[style=XMLStyle]
<EXPANSIONS>
    <E COMPOSITE="C[0]" NUMMODES="10" FIELDS="u,v,T,p" 
    TYPE="GLL_LAGRANGE_SEM" />
</EXPANSIONS>.
\end{lstlisting}
%
To finalise the problem definition and facilitate solution generation, we define a section named \texttt{CONDITIONS} within the session file. Specifically, the \texttt{CONDITIONS} section encompasses the following entries:
\begin{enumerate}
\item \textbf{Solver information} (\texttt{SOLVERINFO}) such as the 
equation, the projection type, the evolution operator, and the analysis driver to use, along with other properties. The solver properties are specified as quoted attributes and have the form
%
\vspace{0.5cm}
\begin{lstlisting}[style=XMLStyle]
<SOLVERINFO>
    <I PROPERTY="[STRING]" VALUE="[STRING]" />
    ...
</SOLVERINFO>    
\end{lstlisting}
%
\begin{tutorialtask}
In the \texttt{SOLVERINFO} section of \texttt{rbc-DNS.xml}:
\tutorialnote{The bits to be completed are identified by \ldots in this file.}
\begin{itemize}
    \item set the \texttt{EvolutionOperator} to \texttt{Nonlinear} 
    in order to select the equations (\ref{eq:u})--(\ref{eq:div_u}),
    \item set the \texttt{Projection} property to \texttt{Continuous} 
    in order to select the continuous Galerkin approach, 
    \item set the \texttt{Driver} to \texttt{Standard} in order 
    to perform standard time-integration.
\end{itemize}
\end{tutorialtask}
%
\item The \textbf{parameters} (\texttt{PARAMETERS}) are specified as name-value 
pairs:
\begin{lstlisting}[style=XMLStyle]
<PARAMETERS>
    <P> [KEY] = [VALUE] </P>
    ...
</PARAMETERS>    
\end{lstlisting}
%
\begin{tutorialtask}
Declare the two parameters \texttt{Ra}, that represents the Rayleigh number, 
and \texttt{Pr}, which represents the Prandtl number. Now set the 
Rayleigh number to 5000 and the Prandtl number to $0.71$ - i.e.\\[1em]
\texttt{<P> Ra = 5000 </P>}\\
\texttt{<P> Pr = 0.71 </P>}\\[1em]
\end{tutorialtask}
%
\item The declaration of the \textbf{variable(s)} (\texttt{VARIABLES}).  
\begin{lstlisting}[style=XMLStyle]
<VARIABLES>
            <V ID="0"> u </V>
            <V ID="1"> v </V>
            <V ID="2"> T </V>
            <V ID="3"> p </V>
</VARIABLES>
\end{lstlisting}
\item Defining \textbf{boundary regions} (\texttt{BOUNDARYREGIONS}) involves specifying composites from the \texttt{GEOMETRY} section and the associated conditions applied to these boundaries (\texttt{BOUNDARYCONDITIONS}). The format for \textbf{boundary conditions} imposed on a region must be in accordance with the conditions outlined for each variable in the \texttt{VARIABLES} section to establish a well-posed problem. The \texttt{REF} attribute associated with a boundary condition region ought to align with the \texttt{ID="[INDEX]"} of the intended \textbf{boundary region} outlined in the \texttt{BOUNDARYREGIONS} section. For example, to model a hot bottom plate corresponding to \texttt{REF="0"}, we set the following condition:
%
\begin{lstlisting}[style=XMLStyle]
<REGION REF="0">
    <D VAR="u" VALUE="0" />
    <D VAR="v" VALUE="0" />
    <D VAR="T" VALUE="1" />
    <N VAR="p" USERDEFINEDTYPE="H" VALUE="0" />
</REGION>
\end{lstlisting}
%
\begin{tutorialtask}
Model the top-cooled plate, which corresponds to \texttt{REF="1"} in the session file.
\end{tutorialtask}
%
\item The specification of (time- and) space-dependent functions (\texttt{FUNCTION}), involves defining functions in terms of $x$, $y$, $z$, and $t$, encompassing aspects like initial conditions and forcing functions. The \texttt{VARIABLES} denote the components of the particular function in a specified direction.
%
\begin{lstlisting}[style=XMLStyle]
<FUNCTION NAME="[NAME]">
    <E VAR="[VARIABLE_1]" VALUE="[EXPRESSION]"/>
    <E VAR="[VARIABLE_2]" VALUE="[EXPRESSION]"/>
    ...
</FUNCTION>
\end{lstlisting}
%
\begin{tutorialtask}
Define a function called \texttt{InitialConditions}. The initial condition can be derived from the solution of the conduction equation for the temperature field in the absence of fluid motion. Therefore, the initial condition will be as follows:
\begin{align}
u&=0 \\
v&=0 \\
T&=1-y \\
p&=0
\end{align}
\end{tutorialtask}
\end{enumerate}
%
Next, we apply the buoyancy force in the vertical direction, with gravity acting downward. This is implemented by defining the following function as the body force.
%
\begin{lstlisting}[style=XMLStyle]
<FUNCTION NAME="BodyForce">
	<E VAR="u" VALUE="0" EVARS="uvT">
	<E VAR="v" VALUE="Ra*Pr*T" EVARS="uvT" />
	<E VAR="T" VALUE="0" EVARS="uvT"  />
</FUNCTION>
\end{lstlisting}
%
Note that for using the body force you need the following additional  tag outside the section \texttt{CONDITIONS}:
%
\begin{lstlisting}[style=XMLStyle]
<FORCING>
    <FORCE TYPE="Body">
        <BODYFORCE> BodyForce </BODYFORCE>
    </FORCE>
</FORCING>
\end{lstlisting}
%
We also wish to monitor the simulation progress. To achieve this, we plan to record flow quantities at the location $(x, y) = (1.0, 0.5)$. For this purpose, we incorporate a filter called \texttt{HistoryPoints} in the session file as follows:
%
\begin{lstlisting}[style=XMLStyle]
    <FILTERS>
        <FILTER TYPE="HistoryPoints">
            <PARAM NAME="OutputFile">rbc</PARAM>
            <PARAM NAME="OutputFrequency">10</PARAM>
            <PARAM NAME="Points">
                1.0 0.5 0
            </PARAM>
        </FILTER>
    </FILTERS>
\end{lstlisting}
%
This completes the specification of the problem.
%
\begin{tutorialtask}
Run the solver by typing the following command on the command line:
\tutorialcommand{\$NEK/IncNavierStokesSolver rbc-DNS.xml}
\end{tutorialtask}
%
Now, after completing the calculation, we wish to verify if the solution has reached a steady state. To do this, we plot the time history of $v(t)$ at $(x, y) = (1.0, 0.5)$, displayed in figure~\ref{time_history}. As evident from the plot, the solution has reached a steady state. It is worth noting that you can use any plotting software for this analysis.
\begin{figure}
\centering
\includegraphics[scale=0.7]{img/plot.pdf}
\caption{Time history of $v(t)$ velocity component measured at $(x,y) = (1.0, 0.5)$.}
\label{time_history}
\end{figure}
%
Next, we want to visualise the steady solution. Specifically, we need to convert the \texttt{.fld} file into a format readable  by a visualisation post-processing tool. In this tutorial we decided to convert the 
\texttt{.fld} file into a VTK format and to use the open-source visualisation package called {\em Visit}. 
%
\begin{tutorialtask}
Convert the file:
\tutorialcommand{\$NEK/FieldConvert rbc-DNS.xml rbc-DNS.fld rbc-DNS.vtu}
Now open {\em Visit} and use File -\textgreater Open, to select the VTK file. Next, you can plot different fields.
\end{tutorialtask}
%%%%
Figure~\ref{DNS_2D} displays the density plots of the temperature field and the velocity vector field in the $x-y$ plane.
%
\begin{figure}
\centering
\includegraphics[scale=0.4]{img/RBC_2D_Solution.png}
\caption{Converged solution for $Ra=5000$ and $Pr=0.71$.}
\label{DNS_2D}
\end{figure}


\section*{Nusselt number}
To validate our results, we will calculate the Nusselt number ($Nu$), representing the ratio of the total heat flux (convective plus conductive) to the conductive heat flux at the plate. The Nusselt number is expressed as follows\footnote{Gary A. Glatzmaier, {\em Introduction to Modeling Convection in Planets and Stars: Magnetic Field, Density Stratification, Rotation}, Princeton University Press, 2013.}:
\begin{align}
Nu = -\frac{1}{L_x}\int \frac{\partial T}{\partial y} dx.
\end{align}
%
For the calculation of the Nusselt number, we will use the \texttt{FieldCovert} and \texttt{NekMesh} utility. We will follow the following steps:
\begin{enumerate}
\item Extract the wall boundary surface in a \texttt{xml} file using the \texttt{NekMesh} utility as:
\begin{lstlisting}[style=BashInputStyle]
NekMesh -m extract:surf=1 rbc-DNS.xml rbc_surf.xml
\end{lstlisting}
\begin{tipbox}
Remember to change the \texttt{NUMODES} and  the polynomial basis functions according to the session file in \texttt{rbc-DNS.xml}.
\end{tipbox}
\item Compute $\frac{\partial T}{\partial y}$, and remove all unnecessary fields. 

\begin{lstlisting}[style=BashInputStyle]
FieldConvert -m gradient rbc-DNS.xml rbc-DNS.fld temp_1.fld
FieldConvert -m removefield:fieldname="u,v,T,p,u_x,u_y,\\
v_x,v_y,T_x,p_x,p_y" rbc-DNS.xml temp_1.fld dT_dy.fld
\end{lstlisting}
\item Next, extract $\frac{\partial T}{\partial y}$ at the top boundary. 

\begin{lstlisting}[style=BashInputStyle]
FieldConvert -m extract:bnd=0 rbc-DNS.xml dT_dy.fld dT_dy_bnd.fld
\end{lstlisting}

\item Finally, we will compute $\frac{1}{L_x}\int \frac{\partial T}{\partial y} dx$ using \texttt{mean} utility.
\begin{lstlisting}[style=BashInputStyle]
FieldConvert -m mean rbc_surf.xml dT_dy_bnd_b0.fld stdout
\end{lstlisting}
Above command gives the following output:
\begin{lstlisting}[style=BashInputStyle]
Domain length : 2.02
Integral (variable T_y) : -4.26191
Mean (variable T_y) : -2.10986 
\end{lstlisting}
Now since 
$$\frac{1}{L_x}\int \frac{\partial T}{\partial y} dx= -2.10986 \implies Nu = -\frac{1}{L_x}\int \frac{\partial T}{\partial y} dx= 2.10986.$$
\end{enumerate}
Our calculation of the Nusselt number match very well with that of Clever and Busse  (J. Fluid Mech., 1974; \textbf{65}:625-645), which is 2.112. 
%
\begin{tutorialtask}
In the file  \texttt{rbc-DNS.xml} change the values of $Ra$ and  $Pr$, and again run the solver to compute the Nusselt number (your results should match with Table~(\ref{tab:Nu_values}).
\end{tutorialtask}
%
\setlength{\tabcolsep}{20pt}
\begin{table}
\begin{center}
%\def~{\hphantom{0}}
\begin{tabular}{c c c c}
$Ra$  & $Pr$ & $\Delta t$ & $Nu$   \\[2 mm]
$10^4$   & $0.71$&  $5 \times 10^{-4}$ & $2.65179$ \\
$3 \times 10^3$   & $7.0$&  $10^{-3}$ & $1.66236$ 
\end{tabular}
\caption{Nusselt number as a function of Rayleigh number and Prandtl number. Above results matches very well with the Table 1 of Clever and Busse  (J. Fluid Mech., 1974; \textbf{65}:625-645).}
\label{tab:Nu_values}
\end{center}
\end{table}

\begin{center}
\textbf{\Large This completes the tutorial.}
\end{center}


\chapter{Linear Stability Analysis}
\label{RBC:LSA}
In this chapter, we will compute the leading eigenvalues and eigenvectors for Rayleigh--B\'enard Convection (RBC). We have the analytical solution for the base flow, which is simply the solution of the conduction equation for the temperature field in the absence of fluid motion (the same solution used as the initial condition in the previous chapter). Therefore, $\mathbf{U}=0$ and $\bar{T}=1-y$.  Our goal is to reproduce the value of the critical Rayleigh number, $Ra_c= 1707.762$, first predicated by Chandrasekhar\footnote{S. Chandrasekhar, {\em Hydrodynamic and Hydromagnetic Stability}, Clarendon, 1968.}. 

First, we will perform linear stability analysis for $Ra=1900$ and $Pr=0.71$.  In the \texttt{\$NEKTUTORIAL/LSA/Ra\_1900} folder there is a file called \texttt{rbc-LSA.xml}. This file is similar to \texttt{rbc-DNS.xml}, but contains additional instructions to perform the linear stability analysis.
%
\begin{tutorialtask}
Configure the following additional \texttt{SOLVERINFO} options which are 
related to the stability analysis.
\begin{enumerate}
\item set the \texttt{EvolutionOperator} to \texttt{Direct} in order to activate the linearised governing equations (\ref{eq:u_prime})--(\ref{eq:div_u_prime}).
\item set the \texttt{Driver} to \texttt{ModifiedArnoldi} in order to use the {\em ModifiedArnoldi} eigenvalue analysis.
\end{enumerate}
\end{tutorialtask}
%
\begin{tutorialtask}
Set the parameters for the Arnoldi algorithm.
\begin{itemize}
\item \texttt{kdim=8}: dimension of Krylov-space,
\item \texttt{nvec=1}: number of requested eigenvalues,
\item \texttt{nits=500}: number of maximum allowed iterations,
\item \texttt{evtol=1e-7}: accepted tolerance on the eigenvalues  and it determines the stopping criterion of the method.
\end{itemize}
\end{tutorialtask}
Next, we need to define the \texttt{InitialConditions}. We will use the random initial condition, which is implemented by using following within the \texttt{CONDITIONS} section. 
\begin{lstlisting}[style=XMLStyle]
<FUNCTION NAME="InitialConditions">
    <E VAR="u" VALUE="awgn(0.0001)" />
    <E VAR="v" VALUE="awgn(0.0001)" />
    <E VAR="T" VALUE="awgn(0.0001)" />
    <E VAR="p" VALUE="0" />
</FUNCTION>
\end{lstlisting}
%
\begin{tutorialtask}
Define a function called \texttt{BaseFlow}. Set the following function for the base flow:
\begin{align}
U&=0 \\
V&=0 \\
\bar{T}&=1-y \\
P&=0
\end{align}
\end{tutorialtask}
%
\begin{tutorialtask}
Run the solver to perform the analysis
\tutorialcommand{\$NEK/IncNavierStokesSolver rbc-LSA.xml}
\end{tutorialtask}
%
The simulation should converge in 8 iterations, and the terminal screen 
should look similar to the one below at the end:
\begin{lstlisting}[style=BashInputStyle]
EV: 0 1.13325     0           1.25088     0           
Writing: "rbc-LSA_eig_0.fld" (0.015323s, XML)
\end{lstlisting}
%
\begin{tutorialtask}
Plot the leading eigenvector in \paraview or {\em VisIt}. This should look like the
solution shown in figures \ref{rbc_eig}. 
\end{tutorialtask}
 %
\begin{figure}
\centering 
\includegraphics[scale=0.4]{img/eig_0.png}
%\includegraphics[trim=80 0 0 0,clip,width=0.48\textwidth]{img/cyl_eig_v.png}
\caption{The velocity vector field $\mathbf{u}^\prime$ on top of the density plots of $T^\prime$ for the leading eigenmode.}
\label{rbc_eig}
\end{figure}
%
\section{Estimate the value of critical Rayleigh number}
\begin{tutorialtask}
Now again run the linear stability analysis for $Ra = 1600, 1700, 1800$. Value of growth rates for these simulation are given in Table~\ref{tab:sigma_values}.  
\end{tutorialtask}
%
\setlength{\tabcolsep}{20pt}
\begin{table}
\begin{center}
%\def~{\hphantom{0}}
\begin{tabular}{c c}
$Ra$  & $\sigma$   \\[2 mm]
$1600$   & $-0.731021$\\
$1700$ &  $-0.0519525$\\
$1800$ & $0.608177$
\end{tabular}
\caption{Growth rate of the most unstable mode  as a function of Rayleigh number.}
\label{tab:sigma_values}
\end{center}
\end{table}
%
\begin{tutorialtask}
Use linear interpolation to estimate the Rayleigh critical. You should get $Ra_c = 1707.87$, which makes a good agreement with the value obtained by Chandrasekhar (1961). 
\end{tutorialtask}
%
For linear interpolation you can use following Python script:
\begin{lstlisting}[style=BashInputStyle]
In [1]: Ra = np.array([1600, 1700, 1800, 1900])

In [2]: sigma = np.array([-0.731021, -0.0519525, 0.608177, 1.25088])

In [3]: np.lnterp(0, sigma, Ra)

Out[3]: 1707.8700467105318
\end{lstlisting}
\begin{center}
%
\textbf{\Large This completes the tutorial.}
\end{center}
\end{document}
